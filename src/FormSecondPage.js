import React from 'react';
import PostFormAdress from './components/PostFormAdress';
import axios from "axios";
import Navbar from "./components/FormPersonalNavbar";


class OrderForm extends React.Component {
  constructor(props) {
      super(props);

      this.companyId = this.props.match.params.companyId;
      this.state = {
          companies: [],

      };
  }


  componentDidMount() {
      axios.get('http://ipmedt4.applepi.nl/api/company').then(res => {
        this.setState({ companies: res.data});
      });

  }

  render() {

    return(
			<div className="OrderForm">
        <Navbar />


			{<PostFormAdress key={this.companyId} id={this.companyId} />}
				{/* <PostList /> */}
			</div>
		)
  }
}

export default OrderForm;
