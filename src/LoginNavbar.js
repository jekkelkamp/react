import React from 'react';
import './NavBar.css';
import {Link} from 'react-router-dom';

              //NavBar is a component which returns a navbar, the routes used below are declared in App.js with Route
class LoginNavbar extends React.Component {
        render(){
            return (
                <nav className="navbar">

                  <div className="navbar--div">
                  <Link className="navbar--link__order" to="/">
                  <h4 className="navbar--login">Home</h4>
                  </Link>
                  <h3 className="navbar--title">AlphensBezorgd</h3>
                  </div>
                </nav>


  );
}

}

export default LoginNavbar;
