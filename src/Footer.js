import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import RestoreIcon from '@material-ui/icons/Restore';
import SearchIcon from '@material-ui/icons/Search';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import HomeIcon from '@material-ui/icons/Home';


const useStyles = makeStyles({
  root: {
    width: 500,
  },
});

export default function SimpleBottomNavigation() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  return (
    <footer>
    <BottomNavigation
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);

      }}

    >
      <BottomNavigationAction className="HomeIcon"  icon={<HomeIcon />} />
      <BottomNavigationAction  icon={<SearchIcon />} />
      <BottomNavigationAction  icon={<LocationOnIcon />} />
      <BottomNavigationAction  icon={<FavoriteIcon />} />
    </BottomNavigation>
    </footer>
  );
}
