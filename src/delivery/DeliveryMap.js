import React, { Component } from "react";
import { Map, TileLayer } from "react-leaflet";
import L from "leaflet";
import "leaflet-routing-machine";
import 'leaflet-geometryutil';
import "./DeliveryMap.css";
import {sortingAlgorithm} from './DeliveryMapCalculations';
import Axios from 'axios';
import NavBar from '../NavBar';

const center = { lat: 52.472211, lng: 6.467973 };

class MapExample extends Component {
  state = { points: []};

  componentDidMount() {
    const map = this.leafletMap.leafletElement;
    let waypoints = []

    //this wil add the routing control to the map.
    var geocoder = L.Control.Geocoder.photon(),
        routeControl = L.Routing.control({
            plan: L.Routing.plan(waypoints, {
                    createMarker: function(i, wp, nWps) {

                      // defining custom icons
                        var delivererIcon = L.icon({
                          iconUrl: 'icons/deliverer.png',
                          iconAnchor: [24,30],
                          iconSize:     [40, 40],
                        })
                        var wpt1 = L.icon({
                          iconUrl: 'icons/wpt1.png',
                          iconAnchor: [24,30],
                          iconSize:     [40, 40],
                        })
                        var wpt2 = L.icon ( {
                          iconUrl: 'icons/wpt2.png',
                          iconAnchor: [24,30],
                          iconSize:     [40, 40],
                        })
                        var wpt3 = L.icon ( {
                          iconUrl: 'icons/wpt3.png',
                          iconAnchor: [24,30],
                          iconSize:     [40, 40],
                        })
                        var wpt4 = L.icon ( {
                          iconUrl: 'icons/wpt4.png',
                          iconAnchor: [24,30],
                          iconSize:     [40, 40],
                        })

                        //Assigning icons based on order in waypoints array.
                        if(i === 0){
                          return L.marker(wp.latLng, {
                            draggable: false,
                            icon: delivererIcon,
                          })
                        } else if(i === 1) {
                          return L.marker(wp.latLng, {
                            draggable: false,
                            icon: wpt1,
                          })
                        } else if(i === 2) {
                          return L.marker(wp.latLng, {
                            draggable: false,
                            icon: wpt2,
                          })
                        }else if(i === 3) {
                          return L.marker(wp.latLng, {
                            draggable: false,
                            icon: wpt3,
                          })
                        }else if(i === 4) {
                          return L.marker(wp.latLng, {
                            draggable: false,
                            icon: wpt4,
                          })
                        }

                    },
                geocoder: geocoder,
                routeWhileDragging: false,
                addWaypoints: false,
                draggableWaypoints: false,
                fitSelectedRoutes: false,
                showAlternatives: false,
            })
        }).addTo(map);


        //Setting deliverer location
        let delivererLocationLat = 52.476579;
        let delivererLocationLon = 6.494644;
        let delivererLocation = new L.latLng(delivererLocationLat,delivererLocationLon);
        waypoints.push(delivererLocation);
        // 
        // let test1 = 52.469604;
        // let test2 = 6.472555;
        // let test3 = new L.latLng(test1,test2);

        //now blank, but will later on be pushed with objects that contain delivery information
        let deliveries = [];

        //now blank, but will later on be pushed with objects that contain pickup information
        let pickups = [];

        let waypointOrderSorted = [];

        let deliveryByDelivererId = 'https://ipmedt4.applepi.nl/api/delivery/by-deliverer-id/';


        //API calls to populate arrays with latest data
        Axios.get(deliveryByDelivererId + 1)
        .then((response) => {
          console.log(response);
          //for every order the deliverer has, push the addres (formatted below) to the deliveries & pickups array
          for(let i = 0; i !== response.data.get_deliveries.length; i ++) {
            console.log(i);
            let deliveryAddres =
              response.data.get_deliveries[i].getDeliveryLocation.streetname + " " +
              response.data.get_deliveries[i].getDeliveryLocation.house_number +
              response.data.get_deliveries[i].getDeliveryLocation.house_number_addition + " " +
              response.data.get_deliveries[i].getDeliveryLocation.postal_code;

            let pickupAddres =
              response.data.get_deliveries[i].get_company_location.streetname + " " +
              response.data.get_deliveries[i].get_company_location.house_number +
              response.data.get_deliveries[i].get_company_location.house_number_addition + " " +
              response.data.get_deliveries[i].get_company_location.postal_code;

            let deliveryObject = {name:'Klant', type:'delivery', orderId:response.data.get_deliveries[i].order_id, addres:deliveryAddres, distanceFromStartPoint:0, processed:false, lat:0, lon:0};
            let pickupObject = {name:response.data.get_deliveries[i].get_company_info.name, type:'pickup', orderId:response.data.get_deliveries[i].order_id, addres:pickupAddres, distanceFromStartPoint:0, processed:false, lat:0, lon:0};

            deliveries.push(deliveryObject);
            pickups.push(pickupObject);
            console.log(deliveryObject);
            console.log(pickupObject);
          }
        }).then(() => {


        //calculates distance between delivererlocation and lat lon from array
        const calculateDistanceFromArray = (array) => {
          return new Promise( (resolve) => {
            for(let i = 0; i !== array.length; i ++) {

              //geocodes the addres in the array to coordinates (lat/lng)
              geocoder.geocode(array[i].addres, (a,b) => {
                array[i].lat = a[0].center.lat;
                array[i].lon = a[0].center.lng;
                let thisLatLong = L.latLng(a[0].center.lat,a[0].center.lng);

                //conferts the distance to KM (from meter)
                let distance = L.GeometryUtil.distance(map,delivererLocation,thisLatLong) / 1000;
                array[i].distanceFromStartPoint = distance;
                console.log(distance);

                if(array.length === 1){
                  resolve();
                }
                //resolve only when the last object (2) is geocoded
                else if( i === 1) {
                  resolve()

                }
              })
            };
          })
        }

        //calls on the calculateDistanceFromArray function to calculate the distance from the two arrays
        const calculateDistances = () => {

          return new Promise( (resolve) => {

            //synchronizing the functions to wait until every geocode is ready, then returns promise
            calculateDistanceFromArray(pickups).then( () => {
              calculateDistanceFromArray(deliveries).then( () => {
                resolve()
                console.log('resolved');
              })
            })

          })

        }


        //waits for the calculateDistance function to finish then sorts the array seperately, by distance from deliverer ASC


        calculateDistances().then( () => {
          console.log('before sort');
          deliveries.sort(function(a, b) {
            var keyA = a.distanceFromStartPoint,
              keyB = b.distanceFromStartPoint;
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
          });
          pickups.sort(function(a, b) {
            var keyA = a.distanceFromStartPoint,
              keyB = b.distanceFromStartPoint;
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
          });

        });


        calculateDistances().then( () => {
          //first add the closest pickup and set processed to true
          waypointOrderSorted.push(pickups[0]);
          pickups[0].processed = true;

          console.log(pickups);
          if(pickups.length !== 1){
            sortingAlgorithm(deliveries,pickups,waypointOrderSorted);
          }else{
            waypointOrderSorted.push(deliveries[0]);
          }



          const setWaypoints = () => {
            return new Promise( (resolve) => {
              console.log(waypointOrderSorted);
              for(let i = 0; i !== waypointOrderSorted.length; i++) {
                    let wpt = L.Routing.waypoint(L.latLng(waypointOrderSorted[i].lat,waypointOrderSorted[i].lon));
                    waypoints.push(wpt);
                    this.setState({
                         points: [...this.state.points, waypointOrderSorted[i] ]
                       })
              }
              resolve()
            })
          }

          setWaypoints().then( () => {
            routeControl.setWaypoints(waypoints);
            document.getElementById('js--loadingModal').style.display = "none";
          })

        })



      });


        //hides the waypoint details by default
        routeControl.hide();

        // FOR TEST PURPOSES, updates the location of the deliverer (first waypoint)
        // let testUpdate = setTimeout( () => {
        //   routeControl.spliceWaypoints(0, 1, delivererLocation);
        //   console.log('updating location');
        // }, 3000);
        //
        // let testUpdate2 = setTimeout( () => {
        //   routeControl.spliceWaypoints(0, 1, test3);
        //   console.log('updating location');
        // }, 6000);

}
  render() {

    return (
      // Here the map is actually created
      <main>
      <NavBar />
      <article className="deliveryMapContainer">
          <Map className="deliveryMapContainer--map"
            center={center}
            zoom={13}
            ref={m => {
              this.leafletMap = m;
            }}
          >
            <TileLayer
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            />
          </Map>

          <article>
            <header>
              <h2 className="routeInfoHeader">Route Info</h2>
            </header>
            <section className="routeInfo">
              {this.state.points.map(point => <section key={point.lat} className="routeInfo--item"><p key={point.lon}>{point.name}</p> <p key={point.addres}> {point.addres} </p> </section>)}
            </section>
          </article>

          <article id="js--loadingModal" className="loadingModal">
            <div className="loadingModal--spinner"></div>
          </article>
      </article>
      </main>




    );
  }
}

export default MapExample;
