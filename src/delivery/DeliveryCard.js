import React from 'react';
import './DeliveryCard.css';

const DeliveryCard = (props) => {




  return (

    <article className="deliveryCard">
      <h3 className="deliveryCard--title">Bezorging van {props.companyName || "<<Bedrijfsnaam>>"}</h3>
      <section className="deliveryCard__body">
        <section className="deliveryCard__body__item">
          <h4 className="deliveryCard__body__item--title">VAN</h4>
          <p>{props.addressFrom || "<<Bedrijfs adres>>"}</p>
        </section>

        <section className="deliveryCard__body__item">
          <p className="deliveryCard__body__item--between"> ⟷ <br/> {props.distance || "Berekenen.."}<br/> €2,50 </p>
        </section>

        <section className="deliveryCard__body__item">
          <h4 className="deliveryCard__body__item--title">NAAR</h4>
          <p>{props.addressTo || "<<Klant adres>>"}</p>
        </section>



      </section>
      <footer className="deliveryCard__footer">
        <button className="deliveryCard__footer--button" onClick={props.onClick}>Accepteren</button>
      </footer>

    </article>
  )
}

export default DeliveryCard;
