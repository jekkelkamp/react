import React from "react";
import Axios from 'axios';
import L from "leaflet";
import './DeliveryOverview.css';
import DeliveryCard from './DeliveryCard';
import DeliveryPopup from './DeliveryPopup';
import {calculateDistance} from './DeliveryMapCalculations';
import NavBar from '../NavBar';

//This component returns multiple 'deliverycards' with the available deliveries
class DeliveryOverview extends React.Component {
  state = {
    deliveriesAll: [],
    deliveriesSorted: [],
    delivererId: 1,
    delivererLatLng: 0,
    showConfirmationPopup: false,
    showSecondDeliveryPopup: false,
    confirmationPopupText: '',
    secondDeliveryPopupText: '',
    firstOrderId: 0,
    firstOrder: 0,
    secondOrderId: 0,
    secondOrder: 0,
  };


  toggleConfirmationPopup() {
      this.setState({
        showConfirmationPopup: !this.state.showConfirmationPopup
      });
  }

  toggleSecondDeliveryPopup() {
      this.setState({
        showSecondDeliveryPopup: !this.state.showSecondDeliveryPopup
      });
  }


  selectOrder(deliveryId,delivererId,name){
    console.log(this.state.firstOrder);
    console.log(this.state.secondOrder);
    if(this.state.firstOrder === 0) {
      console.log('spoof');
      let text = "Weet je zeker dat je de bestelling van \n"+name+" wil accepteren?"
      this.setState({
        confirmationPopupText: text,
        firstOrder: deliveryId,
      });
      this.toggleConfirmationPopup();
    }else if(this.state.firstOrder !== 0 && this.state.secondOrder === 0){
      this.toggleConfirmationPopup();
      let text = "Er is nog een bestelling in de buurt van "+name+" wil je deze ook accepteren?"
      this.setState({
        secondDeliveryPopupText: text,
        secondOrder: 2,
      });
      this.toggleSecondDeliveryPopup();
    }
  }

  confirmationPopup(deliveryId, order, name) {
    let text = "Weet je zeker dat je de bestelling van "+name+" wil accepteren?"
    this.setState({
      confirmationPopupText: text,
      firstOrderId: deliveryId,
      firstOrder:order
    });
    this.toggleConfirmationPopup();
  }

  //wil make an API call to assign a deliverer to a delivery
  assignDeliverer(deliveryId,delivererId){
    return new Promise( (resolve) => {
      let ASSIGN_URL = "https://ipmedt4.applepi.nl/api/delivery/assign/" + deliveryId + "/" + delivererId;
      Axios.put(ASSIGN_URL).then( (response) => {
        console.log(response);
        resolve();
      }).catch((error) => {
        alert(error);
      })
    });
  }

  calculateSecondOrder(){

    for(let i = 0; i != this.state.deliveriesSorted.length; i ++) {
      setTimeout(() =>{
        calculateDistance(this.state.firstOrder.formatted_address_from,this.state.deliveriesSorted[i].formatted_address_from).then((result) => {
          if(result > 0 && result < 2.0 && this.state.secondOrder === 0) {
            this.setState({
              secondOrderId: this.state.deliveriesSorted[i].id,
              secondOrder: this.state.deliveriesSorted[i],
            });
          }
        }).then(() => {
          if(this.state.secondOrder !== 0) {
            this.toggleConfirmationPopup();
            let text = "Er is nog een bestelling in de buurt van "+this.state.secondOrder.orderedByCompany.name+" wil je deze ook accepteren?"
            this.setState({
              secondDeliveryPopupText: text,
              secondOrder: 2,
            });
            this.toggleSecondDeliveryPopup();
          }
        })
      }, i * 1000);
    }

  }



  proceed(status) {
    switch(status){
      case 'oneOrder':
        this.assignDeliverer(this.state.firstOrderId,this.state.delivererId).then(() => {
          window.location.replace("/deliverymap");
        });
        break;

      case 'twoOrder':
        this.assignDeliverer(this.state.firstOrderId,this.state.delivererId).then(() => {
          this.assignDeliverer(this.state.secondOrderId,this.state.delivererId).then(() => {
            window.location.replace("/deliverymap");
          });
        });
        break;
    }
  }

  test(deliveryId,delivererId,name) {
    this.selectOrder(deliveryId,delivererId,name);
    this.assignDeliverer(this.state.firstOrder,1);
    this.assignDeliverer(this.state.secondOrder,1);
  }

  componentDidMount() {

    //get deliverer location and cast to latlng
    Axios.get('https://ipmedt4.applepi.nl/api/deliverer/by-deliverer-id/1').then(res=> {
      let delivererLatLng = new L.latLng(res.data.lat,res.data.lon);
      this.setState( {
        delivererId: res.data.id,
        delivererLatLng: delivererLatLng,
      })
    });

    //get all deliveries
    Axios.get('https://ipmedt4.applepi.nl/api/delivery/unassigned').then(res => {
      console.log(res);
      this.setState({
        deliveriesAll: res.data,
      });
    }).then(() => {
      for(let i = 0; i < this.state.deliveriesAll.length; i ++){

        //wil format the from and to addres and force update it to display in cards
        this.state.deliveriesAll[i].formatted_address_from = this.state.deliveriesAll[i].orderedByCompany.addres.streetname + " " + this.state.deliveriesAll[i].orderedByCompany.addres.house_number + this.state.deliveriesAll[i].orderedByCompany.addres.house_number_addition + " " + this.state.deliveriesAll[i].orderedByCompany.addres.postal_code;
        this.state.deliveriesAll[i].formatted_address_to = this.state.deliveriesAll[i].deliverLocation.streetname + " " + this.state.deliveriesAll[i].deliverLocation.house_number + this.state.deliveriesAll[i].deliverLocation.house_number_addition + " " + this.state.deliveriesAll[i].deliverLocation.postal_code;
        this.forceUpdate();



        //makes an array with only the deliveries in a radius of x
        let sortAll = () => {
          return new Promise( (resolve) => {

            //sets timeout so the geocoder server doesnt reject the requests
            setTimeout(() =>{
              //calculates the distance between the deliverer and the pickup point
              calculateDistance(this.state.delivererLatLng,this.state.deliveriesAll[i].formatted_address_from).then((result) => {
                let value = this.state.deliveriesAll[i];

                //if the result is below the vlaue X then add it to the array which will later display the deliveries
                if(result < 5) {
                  console.log(value);
                  this.setState({
                    deliveriesSorted: [
                      ...this.state.deliveriesSorted,
                      value
                    ]
                  });
                  this.forceUpdate();
                  resolve();
                }
              });
            }, i * 2000);
          })//promise
        }//sortAll
        sortAll().then(() => {
          for(let i = 0; i != this.state.deliveriesSorted.length; i ++) {
            //calculates the distance between the from and to addres, then force updates it to display in cards
            calculateDistance(this.state.deliveriesSorted[i].formatted_address_from,this.state.deliveriesSorted[i].formatted_address_to).then((result) => {
              this.state.deliveriesSorted[i].distance = result + " KM";
              this.forceUpdate();
            });
          }
        });

      }//forloop
    });//then
  }
  render() {
    return (
      <main className="DeliveryOverview">
      <NavBar/>
      <p className="DeliveryOverview--title">Hier staat een overzicht van nog niet geaccepteerde bestellingen binnen een radius van 5km vanaf uw locatie</p>
        {this.state.deliveriesSorted.map(delivery =>
          <DeliveryCard
            key={delivery.id}
            deliveryId={delivery.id}
            delivererId={this.state.delivererId}
            companyName={delivery.orderedByCompany.name}
            addressFrom={delivery.formatted_address_from}
            addressTo={delivery.formatted_address_to}
            distance={delivery.distance}
            onClick={() => this.confirmationPopup(delivery.id, delivery, delivery.orderedByCompany.name)}

          />
        )}
        {this.state.showConfirmationPopup ?
        <DeliveryPopup
          closePopup={() => this.toggleConfirmationPopup()}
          acceptOrder={() => this.calculateSecondOrder()}
          text = {this.state.confirmationPopupText}
        />
        : null
        }

        {this.state.showSecondDeliveryPopup ?
        <DeliveryPopup
          closePopup={() => this.proceed('oneOrder')}
          acceptOrder={() => this.proceed('twoOrder')}
          text = {this.state.secondDeliveryPopupText}
        />
        : null
        }
      </main>
    );
  }
}

export default DeliveryOverview;
