
import L from "leaflet";
import "leaflet-routing-machine";
import 'leaflet-geometryutil';
import './geocoder.js';
// var map = L.map('map').setView([42.35, -71.08], 13);
//
// // load a tile layer
// L.tileLayer('http://tiles.mapc.org/basemap/{z}/{x}/{y}.png',
//   {
//     attribution: 'Tiles by <a href="http://mapc.org">MAPC</a>, Data by <a href="http://mass.gov/mgis">MassGIS</a>',
//     maxZoom: 17,
//     minZoom: 9
//   }).addTo(map);
  var geocoder = L.Control.Geocoder.photon();


  export const testie = () => {
    console.log(geocoder);
  }

  //this code will calculate the distance between two points
  export const calculateDistance = (inputA, inputB) => {
    let pointA;
    let pointB;
    return new Promise( (resolve) => {
      //first check if the input is a string (an addres) or a latLng object
      let step1 = () => {
        return new Promise( (resolve) => {
          switch(typeof inputA) {
            case 'string':
              //geocodes a string e.g. longstreet4 1212PE, and casts it to coordinates
              geocoder.geocode(inputA, (a,b) => {
                pointA = new L.latLng(a[0].center.lat,a[0].center.lng);
                resolve();
              });
              break;
            case 'object':
              //if it's already a latLng object just set the value
              pointA = inputA;
              resolve();
              break;
          }
        })
      }

      let step2 = () => {
        return new Promise( (resolve) => {
          switch(typeof inputB) {
            case 'string':
              //geocodes a string e.g. longstreet4 1212PE, and casts it to coordinates
              geocoder.geocode(inputB, (a,b) => {
                pointB = new L.latLng(a[0].center.lat,a[0].center.lng);
                resolve();
              });
              break;
            case 'object':
              //if it's already a latLng object just set the value
              pointB = inputB;
              resolve();
              break;
          }
        })
      }

      //sync this function
      step1().then(() => {
        step2().then(() => {
          // calculates distance between points and devides by 1000 to cast to KM and round it to one decimal
          let result = Math.round(L.GeometryUtil.length([pointA,pointB]) / 1000 * 10) /10;
          resolve(result);
        })
      })
    })
  }



//this hideous piece of code wil make a sorted array based on multiple contraints
  export const sortingAlgorithm = (deliveries, pickups, destination) => {
    new Promise( (resolve) => {
      for(let i = 0; i !== 3; i ++ ) {

        // if the order id of the delivery equals the the order id on the first pickup and if the distance is smaller than the second pickup add it to the array:
        if(deliveries[0].orderId === pickups[0].orderId && deliveries[0].distanceFromStartPoint < pickups[1].distanceFromStartPoint && deliveries[0].processed === false) {
          deliveries[0].processed = true;
          destination.push(deliveries[0]);
        }

        // if the order id of the delivery equals the order id on the first pickup and if the distance is bigger than the second pickup add the following pickup to the array:
        else if(deliveries[0].orderId === pickups[0].orderId && deliveries[0].distanceFromStartPoint > pickups[1].distanceFromStartPoint && deliveries[0].processed === false) {
          pickups[1].processed = true;
          destination.push(pickups[1]);
        }

        //if the second item in the sorted array equals a delivery and the last pickup is not yet processed add it to the array
        else if(destination[1].type === 'delivery' && pickups[1].processed === false) {
          pickups[1].processed = true;
          destination.push(pickups[1]);
        }

        //if the second item in the sorted array equals a pickup and the next delivery is not yet processed add it to the array
        else if(destination[1].type === 'pickup' && deliveries[0].processed === false) {
          deliveries[0].processed = true;
          destination.push(deliveries[0]);
        }

        else if(destination[2].type === 'delivery' && pickups[1].processed === false) {
          pickups[1].processed = true;
          destination.push(pickups[1]);
        }

        else if(destination[2].type === 'pickup' && deliveries[1].processed === false ){
          deliveries[1].processed = true;
          destination.push(deliveries[1]);
        }else{console.log('unexpected result :(');}
      }
      resolve();
    })
  }

  // <div id="map" style="height: 300px"></div>
