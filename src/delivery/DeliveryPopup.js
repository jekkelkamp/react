import React from 'react';
import './DeliveryPopup.css';
const DeliveryPopup = (props) => {

  return (
    <article className="deliveryPopup">
      <section className="deliveryPopup__body">
        <main className="deliveryPopup__body__content">
          <p className="deliveryPopup__body__content--text">{props.text || "text"}</p>
        </main>
        <footer className="deliveryPopup__body__footer">
          <button onClick={props.acceptOrder} className="deliveryPopup__body__footer--buttonAccept">JA</button>
          <button onClick={props.closePopup} className="deliveryPopup__body__footer--buttonDeny">NEE</button>
        </footer>
      </section>
    </article>
  )
}

export default DeliveryPopup;
