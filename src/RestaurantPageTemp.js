import React from "react";
import ProductOverview from "./ProductOverview";

class RestaurantPageTemp extends React.Component {
    render() {
        return (
            <section>
                <ProductOverview companyId={1} />
            </section>
        );
    }
}

export default RestaurantPageTemp;
