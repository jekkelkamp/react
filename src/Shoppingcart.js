import React, { Component } from 'react';
import CartContainer from './Cartcontainer';
import cartItems from './cart-items';
import Navbar from './Shoppingnavbar';
import axios from 'axios';


class Shoppingcart extends Component{

  constructor(props) {
    super(props);

    this.companyId = this.props.match.params.companyId;
    this.state = {
      companies: []
    };
  }

  // Connection with the API
  componentDidMount() {
    axios.get('http://ipmedt4.applepi.nl/api/company').then(res => {
      this.setState({ companies: res.data });



    });
  }
  render(){



    return(
      <main>
      <Navbar />
      <section>
      <CartContainer cartItems={cartItems} key={this.companyId} id={this.companyId}  />

      </section>
      </main>
    );
  }
}

export default Shoppingcart;
