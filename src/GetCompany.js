import React from 'react';
import axios from 'axios';
import AddProduct from './AddProduct';
import './RestaurantOverview.css';
import Navbar from './LoginNavbar';


class GetCompany extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      companies: []
    };
  }

// Connection with the API
  componentDidMount() {
    axios.get('http://ipmedt4.applepi.nl/api/company').then(res => {

      //const locations = {locations: res.data};
      let last = res.data[res.data.length -1];
      this.setState({ companies: last});
      console.log(last);


    });
  }
  render(){
    return(
      <main className="background">
      <Navbar />
      <article >

          <AddProduct id={this.state.companies.id} key={this.state.companies.id} />
          </article>
      </main>
    );
  }

}



export default GetCompany;
