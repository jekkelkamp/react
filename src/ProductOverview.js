import React from 'react';
import ProductCard from './ProductCard';
import axios from 'axios';
import cartItems from "./cart-items";

import RestaurantBanner from './RestaurantBanner';
import "./ProductOverview.css"
import Navbar from './components/ProductNavbarSecond';
import SearchBar from "./SearchBar";



const API = "https://ipmedt4.applepi.nl/api/product/by-company-id/";

let amount = 0;

class ProductOverview extends React.Component {
  state ={
    cart: []
  }
    constructor(props) {
        super(props);

        this.companyId = this.props.match.params.companyId;
        this.state = {
            products: [],
            cart: [],

            isLoading: false,
            error: null,
        };
    }


    componentDidMount() {
        this.getAllProducts();
    }

    getAllProducts = () => {
        this.setState({ isLoading: true });


        // Get all products for company with company ID 1, only for testing purposes.
        axios.get(API + this.companyId)
            .then(res => this.setState({
                products: res.data,
                isLoading: false,
                cart: []
            }))
            // Display error message when the API data could not be fetched.
            .catch(error => this.setState({
                products: [],
                isLoading: false,
                error
            }));
    }

    //Get item by id
    getItem = productId => {
      const product = this.state.products.find(item => item.id === productId);
      return product;
    };
    setProducts = () => {
    let products = [];
    products.forEach(item => {
      const singleItem = { ...item };
      products = [...products, singleItem];
    });
    this.setState(() => {
      return { products };
    }, this.checkCartItems);
  };
  //Add the item to cart by id
    addToCart = (productId, productName, productPrice)  => {
        console.log("Er is geklikt op product met product ID: " + productId);
        console.log(this.state);
        cartItems.push({
        id: productId,
        name: productName,
        price: productPrice,
        amount: 1,
        company_id: this.props.companyId,
        }
      )
          amount = ++amount;

     let cartamount = amount.toString();
     document.getElementById("navbar__total__amount").innerHTML = cartamount;
     console.log("Er is geklikt op product met product ID: " + productId)

      };

    onSearch = (productName) => {
        if (!productName) {
            this.getAllProducts();
        } else {
            this.setState({ isLoading: true });
            axios.get(API + this.companyId + "/product-like-name/" + productName)
            .then(res => this.setState({
                products: res.data,
                isLoading: false
            }))
            .catch(error => this.setState({
                products: [],
                isLoading: false,
                error
            }));
        }
    }

    render() {
        return (
            // Loop over all products received from the API and create cards for them.
            <React.Fragment>
            <main>
            <article>
            <section className="shoppingcart">

            <Navbar key={this.companyId} id={this.companyId}/>
           {<RestaurantBanner key={this.companyId} companyId={this.companyId} />}

           <section className="productoverview_searchbar">
             <SearchBar onSearch={this.onSearch} />
           </section>
           {this.state.products.map((product) =>
                <ProductCard key={product.id}
                    product={product}
                    productid={product.id}
                    productname={product.name}
                    amount={product.amount}
                    productdescription={product.description}
                    allergies={product.allergies}
                    productprice={product.price}
                    company_id={product.company_id}
                    producttotal={product.total}
                    cardClicked={this.addToCart} />

           )}



       </section>
       </article>
       </main>
       </React.Fragment>

);
}
}

export default ProductOverview;
