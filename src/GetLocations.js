import React from 'react';
import axios from 'axios';
import AddCompany from './AddCompany';
import Navbar from './LoginNavbar';
import './RestaurantOverview.css';


class GetLocation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      locations: []
    };
  }

// Connection with the API
  componentDidMount() {
    axios.get('http://ipmedt4.applepi.nl/api/locations').then(res => {
      let last = res.data[res.data.length -1];
      this.setState({ locations: last});
      console.log(last);


    });
  }


  render(){
    return(
      <main>
      <Navbar />
        <article>

          <AddCompany  id={this.state.locations.id} key={this.state.locations.id} />
          </article>
      </main>
    );
  }

}



export default GetLocation;
