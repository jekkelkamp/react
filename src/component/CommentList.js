import React from "react";
import Comment from "./Comment.js"


class CommentList extends React.Component {
  render() {
    return (
      <div>
      {this.props.comments.map((comment, i) => (
      <Comment key={i} text={comment} />
      ))}

      {this.props.comments.map((commentNames, i) => (
      <Comment key={i} text={commentNames} />
      ))}

      </div>
    );
  }
}

export default CommentList;
