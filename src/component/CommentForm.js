import React from "react";


class CommentForm extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      comment: "",
      commentName: ""
    }
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({
      comment: e.target.value,
      commentName: e.target.value
    })
  }

  onSubmit(e) {
    this.props.onSubmit(this.state.comment + " " + this.state.commentName);
    this.setState({
      // Bij het klikken op de knop komt er daarna de comment die hier ingevuld te staan
      comment: "",
      commentName: ""
    });
  }

  render() {
    return (
      <section>
        <textarea value={this.state.comment} onChange={this.handleChange} placeholder="Place your comments here"rows="5" col="30" type="textarea"></textarea>
        <input value={this.state.commentName} onChange={this.handleChange} class="input-name" placeholder="Name"></input>
        <p><span class="counter">Test </span></p>
        <button type="Post" name="button" onClick={this.onSubmit}>Post</button>
      </section>
    );
  }
}

export default CommentForm;
