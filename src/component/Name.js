import React from "react";

class Name extends React.Component {
  render() {
    return <h4>{this.props.textName}</h4>
  }
}

export default Name;
