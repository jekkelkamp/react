import React from 'react';
import './ProductCard.css';
import AddIcon from '@material-ui/icons/Add';

class ProductCard extends React.Component {

addToCart = () => {
    this.props.cardClicked(this.props.productid, this.props.productname, this.props.productprice)

}

render() {
    return (
                  <article className="card">
                      <header className="card__header">
                          <h2> {this.props.productname || "Productnaam"} </h2>
                          <p className="card__header_price">€{this.props.productprice || "Geen"}</p>
                      </header>
                      <section className="card__content">
                          <p className="card__content_description"> {this.props.productdescription || "Productbeschrijving"}</p>
                          <p className="card__content_allergiestext">  Allergieën: {this.props.allergies || "Geen"}</p>

                      </section>
                      <button className="card__button" onClick={this.addToCart}> <AddIcon /> </button>
                  </article>

    );
  }
}

export default ProductCard;
