import React, { Component } from 'react';
import CartItem from './CartItem';
import cartItems from './cart-items';
import axios from 'axios';
import './Cartcontainer.css';
import {Link} from 'react-router-dom';

//Function for showing total of cart
let total = 0;
let cart = [];


class Cartcontainer extends Component{
  componentDidMount() {
    for(let i = 0; i !== cartItems.length; i ++){
      console.log(cartItems[i]);
      total += cartItems[i].price;
      console.log("totaal");
      console.log(total);

      cart.push({amount: cartItems[i].amount,  name: cartItems[i].name, id: cartItems[i].id});
      console.log(cart);
      
    }
  }
  //Switch case for increasing, decrease and removing the cartitem in cart
  updateAmount(where,operator){
    console.log(where);
    switch(operator) {
      case 'increase' :
      //where.amount checks on which cartitem you are on and increases the cartitem with 1
      where.amount += 1;
      this.forceUpdate();
      for(let i = 0; i !== cartItems.length; i ++){
        if(where.id === cartItems[i].id){
          cart[i].amount += 1;
          total += cartItems[i].price;
          total = parseFloat(total.toFixed(2));
        }
        this.state.products_in_order = cart;
      }
      break;

      case 'decrease':
      where.amount -= 1;
      total -= where.price;
      total = parseFloat(total.toFixed(2));
      //this function makes sure it stops when its finished
      return new Promise((resolve) => {
        for(let i = 0; i !== cartItems.length; i ++){
          //if amount of item is 0 or smaller it executes the following
          if(where.id === cartItems[i].id && where.amount <= 0){
            //splice removes on the place that you are
            cartItems.splice(i,1);
            cart.splice(i,1);
            this.state.products_in_order = cart;
            this.state.costs = total;
            resolve();
            break;

          }
          //when cartitem is bigger than 0 it executes this (decreases amount)
          else if(where.id === cartItems[i].id){
            cart[i].amount -= 1;
            this.state.products_in_order = cart;
            this.state.costs = total;
            resolve();
            break;

          }
        }
        this.forceUpdate();
      })
      break;
      //case for remove
      case 'remove':
      this.forceUpdate();
      for(let i = 0; i !== cartItems.length; i ++){
            if(where.id === cartItems[i].id){
              //calculates total by multiplying amount times price
              let amountRemove = total - where.amount * where.price;
              total = amountRemove;
              total = parseFloat(total.toFixed(2));
              //removes item you are on
              cartItems.splice(i,1);
              cart.splice(i,1);

              this.state.products_in_order = cart;
              this.state.costs = total;
            break;
          }
      }
  }

}

  constructor(props) {
    super(props)
    this.state = {
      products_in_order: '',
      ordered_by_company_id: this.props.id,
      payment_method: '',
      delivery_time: '',
      description: '',
      payment_complete: 0,
      costs: total,

    }
  }

//Handles event where input is post in post url
  submitHandler = e => {
    e.preventDefault()
    //
    let submitOrder = '';
    let makeOrder = () => {
      return new Promise( (resolve) => {
        //for-loop makes from array a string
        for(let i = 0; i !== this.state.products_in_order.length; i ++) {
          let value = '' + this.state.products_in_order[i].amount + ' ' + this.state.products_in_order[i].name +' ' ;
          submitOrder += value;
          submitOrder = submitOrder.toString();
          if(i >= 1) {
            resolve();
          }
        };

      })

    }
    makeOrder().then(
      axios.post('https://ipmedt4.applepi.nl/api/order/storeOrder' , {
          products_in_order: submitOrder,
          ordered_by_company_id: this.props.id,
          costs: this.state.costs,
      }).then(response => {
          console.log(response)

        })
        .catch(error => {
          console.log(error)

        })
    );



  }

  render(){

    if (cartItems.length === 0) {
      return (
        <section className="cartcontainer--cart">
          {/* cart header */}
          <header>
            <h1 className="cartcontainer--cart__h1">Winkelmandje</h1>
            <h4 className="cart__empty">is momenteel leeg</h4>
          </header>
        </section>
      );
    }


    return (
      <section className="cartcontainer--cart">
        {/* cart header */}
        <header>
          <h1 className="cartcontainer--cart__h1">Winkelmandje</h1>
        </header>
        {/* cart items */}
        <article className="cartcontainer--cart_items">
          {cartItems.map(name => {
            return <CartItem key={name.id} {...name} functionUp = {() => this.updateAmount(name,'increase')} functionDown = {() => this.updateAmount(name,'decrease')} functionRemove = {() => this.updateAmount(name,'remove')} />;
          })}
        </article>
        {/* cart footer */}
        <footer className="cartcontainer--footer">
          <hr />
          <div className="cartcontainer--cart__total">
            <h3>
              Totaal <span id="cartcontainer--total">€{total}</span>
            </h3>
          </div>

          <button onClick={this.submitHandler} className="cartcontainer--cart__btn__order"><Link className="cartcontainer--link__order" to="/orderingStep/1">
          Bestellen €{total}</Link></button>
        </footer>
      </section>
  )
}
}

export default Cartcontainer;
