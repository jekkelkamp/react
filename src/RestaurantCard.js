import React from 'react';
import "./RestaurantCard.css";
import { Link } from 'react-router-dom';

class RestaurantCard extends React.Component {
  render() {
    return(
      <article className="restaurantCard">
        <figure className="restaurantCard__content__figure">
          <img className="restaurantCard__content__figure__img" src={this.props.imgSrc} alt="the Restaurant" />
        </figure>
        <section className="restaurantCard__content">
          <h1 className="restaurantCard__content__header"> {this.props.title} </h1>

          <p className="restaurantCard__content__text"> {this.props.content || "Check dit gave stukje tekst"} </p>
          <Link to={"/productoverview/" + this.props.id} className="restaurantCard__content__button" > Check it out </Link>
        </section>

      </article>
    );
  }
}

export default RestaurantCard;
