import React from 'react';
import './RestaurantBanner.css';
import axios from 'axios';

const API = "https://ipmedt4.applepi.nl/api/company/by-company-id/";

class OrderBanner extends React.Component {
    constructor() {
        super();
        this.state = {
            showInformationPopup: false,
            showTimesPopup: false,
            showReviews: false,
            companyName: null,
            streetName: null,
            houseNumber: null,
            houseNumberAddition: null,
            postalCode: null,
            isLoading: false,
            error: null,
            times: "10:00 - 22:00"
        };
    }

    componentDidMount() {
        this.setState({ isLoading: true });

        axios.get(API + this.props.companyId)
            .then(res => this.setState({
                companyName: res.data.name,
                streetName: res.data.locations.streetname,
                houseNumber: res.data.locations.house_number,
                houseNumberAddition: res.data.locations.house_number_addition,
                postalCode: res.data.locations.postal_code,
                isLoading: false
            }))
            // Display error message when the API data could not be fetched.
            .catch(error => this.setState({
                error,
                isLoading: false
            }));
    }


    render() {

        return (
            <header className="restaurant_header">
                <h1>{this.state.companyName || "Restaurantnaam"}</h1>
                <h2 >Bestellingen</h2>

            </header>
        );
    }
}

export default OrderBanner;
