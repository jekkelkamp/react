import React from 'react';
import "./Order.css";


class OrderCard extends React.Component {


  render() {
    return(
      <article className="ordercard">
          <header className="ordercard--header">
              <h1 className="ordercard--title">Bestelling</h1>
              <p className="ordercard--header__price">Kosten: €{this.props.costs || "Kosten"}</p>

          </header>
          <section className="ordercard--content">
              <h3 className="ordercard--content__Order">{this.props.order || "Bestelling"} </h3>

              <p className="ordercard--content__deliverytime"> Bezorgtijd: {this.props.deliverytime || "Bezorgtijd"}</p>
              <p className="ordercard--content__paymentmethod">  Betaalwijze: {this.props.paymentmethod || "Betaalwijze"}</p>
              <p className="ordercard--content__description">  Opmerkingen: {this.props.description || "Opmerkingen"}</p>

          </section>

      </article>
    );
  }
}

export default OrderCard;
