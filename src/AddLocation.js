import React from 'react';
import Location from './Location';
import Navbar from './LoginNavbar';
import './RestaurantOverview.css';


// This Component makes a flexbox page and will be call in de RestaurantPage.js were the Navbar will be added
class AddLocation extends React.Component {
  render(){
    return(
      <main>
      <article>
        <Navbar />
      <article >
      <Location />

      </article>
      </article>
      </main>
    );
  }

}



export default AddLocation;
