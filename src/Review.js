import React from 'react';
import PostFormReview from './PostForm';
import Navbar from './NavBar';
import axios from "axios";


class Review extends React.Component {
      constructor(props) {
          super(props);

        this.companyId = this.props.match.params.companyId;
          this.state = {
              companies: [],

          };
      }


      componentDidMount() {
          // Get all products for company with company ID 1, only for testing purposes.
          axios.get('http://ipmedt4.applepi.nl/api/company').then(res => {
            this.setState({ companies: res.data});
          });

      }

  render() {
    return(
			<div className="OrderForm">
        <Navbar />


      <section>

        {<PostFormReview key={this.companyId} id={this.companyId} />}




        </section>



      </div>
		)

  }
}

export default Review;
