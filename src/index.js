import React from 'react';
import ReactDOM from 'react-dom';
import App from "./App";
import "leaflet-control-geocoder/dist/Control.Geocoder.js";


ReactDOM.render(
    <App />,
  document.querySelector("#root")
)
