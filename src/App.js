import React from 'react';
import './App.css';
import axios from 'axios'
import DeliveryMap from './delivery/DeliveryMap'
import PostForm from './Review';
import Navbar from './LoginNavbar';
import Productoverview from './ProductOverview'
import RestaurantOverview from './RestaurantOverview';
import OrderForm from './OrderForm';
import FormSecondPage from './FormSecondPage';
import FormThirdPage from './FormThirdPage';

import Shoppingcart from './Shoppingcart';


import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom';
import DeliveryOverview from './delivery/DeliveryOverview';
import AddLocation from './AddLocation';
import InComingOrders from './IncomingOrders';
import GetLocations from './GetLocations';
import GetCompany from './GetCompany';
import Register from './Register'
import AddOpeningHours from './AddOpeningHours';
import Deliverer_Register from './Register--Deliverer'
import Restaurant_Register from './Register--Restaurant'


import './Login.css';

const fakeAuth = {
  isAuthenticated: false,
  autehnticate(cb) {
    this.isAuthenticated = true
    setTimeout(cb, 100)
  },
  signout(cb) {
    this.isAuthenticated = false
    setTimeout(cb, 100)
  }
}



const Public = () => <Redirect to="/"/>
const Protected = () => <Redirect to="/addlocation"/>


class Login extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      email: '',
      password: '',
      role: '',
      redirectToReferrer: false,
      from: '',
    }
  }

  getUserRole = (user_mail) => {
    return new Promise( (resolve) => {
      axios.get('http://ipmedt4.applepi.nl/api/returnusers')
      .then(response => {
        console.log(response);
        for(let i = 0; i !== response.data.length; i ++){
          if(response.data[i].email === user_mail) {
            console.log(response.data[i].role);
            this.setState( {role: response.data[i].role});
            resolve()
          }
        }
      })
      .catch(error => {
        console.log(error)
      })

    })
  }

  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value})
  }

  //Posts API call to login user
  submitHandler = e => {
    e.preventDefault()
    console.log(this.state)
    axios.post('http://ipmedt4.applepi.nl/api/login', this.state)
      .then(response => {
        console.log(response)

        this.getUserRole(this.state.email).then( () => {
          switch (this.state.role) {
            case 'customer':
              this.setState({from: '/'});
              break;
            case 'deliverer':
              this.setState({from: '/deliveryoverview'});
              break;
            case 'company':
              this.setState({from: '/addlocation'});
              break;
          }
          fakeAuth.autehnticate(() => {
            this.setState(() => ({
              redirectToReferrer: true
            }))
          })

        });

      })
      .catch(error => {
        console.log(error)
      })
  }

  //This is a debug function
  apiHandler = () => {

    console.log("apiHandler wordt aangeroepen")
  }



  render() {
    const { redirectToReferrer, email, password } = this.state



    if (redirectToReferrer === true) {
      return (
        <Redirect to={this.state.from} />
      )
    }

    return (
      <main>
      <Navbar />
      <section className="login--section">
        <section className="login--section__section">
          <form className="login--section__section__from" onSubmit={this.submitHandler}>
            <img src="img/Finallogo.png" className="login--section__section__from__img" alt="Logo AlphensBezorgd"></img>
            <h3 className="login--section__section__from__h3">Login</h3>
            <input
            type="text"
            name="email"
            className="login--section__section__form__input"
            placeholder="email"
            value={email}
            onChange={this.changeHandler}
            >
            </input>
            <input
            type="password"
            name="password"
            className="login--section__section__form__input"
            placeholder="wachtwoord"
            value={password}
            onChange={this.changeHandler}
            >
            </input>
            <button type="submit" onClick={this.apiHandler} className="login--section__section__form__button">Login</button>
            <Link className="login--section__link" to="/register">Registreer als klant</Link>
            <Link className="login--section__link" to="/register--Deliverer">Registreer als bezorger</Link>
            <Link className="login--section__link" to="/register--Restaurant">Registreer als restaurant</Link>
          </form>
        </section>
      </section>
      </main>
    )
  }
}

//Checks if user is authenticated to ensure a privateroute
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    fakeAuth.isAuthenticated === true
      ? <Component {...props} />
      : <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }} />
  )}/>
)

const AuthButton = () => (
  fakeAuth.isAuthenticated === true
  ? <p>
      Welcome! <button onClick={() => ({})}>Sign Out</button>
    </p>
  : <p>You are not logged in.</p>
)

//Details wordt ProductOverview
//productlist wordt restaurantoverview

class App extends React.Component {
  render() {

    return(

      // Route adds web.php like routing, for example '/' (the landing page) rerouts to the home component
      // 'deliverymap' reroutes to the deliverymap component (https://mywebpage.net/deliverymap)
      <div className="App">
        <Router>
          <Route exact path="/public" component={Public} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/register--Deliverer" component={Deliverer_Register} />
          <Route exact path="/register--Restaurant" component={Restaurant_Register} />
          <Route exact path="/" component={RestaurantOverview} />
          <Route exact path="/deliverymap" component={DeliveryMap} />
          <Route exact path="/productoverview/:companyId" component={Productoverview} />
          <PrivateRoute exact path="/addlocation" component={AddLocation} />
          <Route exact path="/addopeningshours" component={AddOpeningHours} />
          <Route exact path="/incomingorders/:companyId" component={InComingOrders} />
          <Route exact path="/companies" component={GetLocations} />
          <Route exact path="/addtoproduct" component={GetCompany} />
          <PrivateRoute exact path="/deliveryoverview" component={DeliveryOverview} />
          <Route exact path="/shoppingcart" component={Shoppingcart}/>
          <Route exact path="/orderingStep/2" component={OrderForm}/>
          <Route exact path="/orderingStep/1" component={FormSecondPage}/>
          <Route exact path="/orderingStep/3" component={FormThirdPage}/>
          <Route exact path="/postform" component={PostForm} />
          <Route exact path="/shoppingcart/:companyId" component={Shoppingcart} />
        </Router>
      </div>
    );
  }
}

export default App;
