import React from 'react';
import axios from 'axios';
import './Order.css';
import OrderCard from "./OrderCard";
import OrderBanner from './OrderBanner';

const API = 'http://ipmedt4.applepi.nl/api/order/by-company-id/';


class InComingOrders extends React.Component {
  constructor(props) {
    super(props);

    this.companyId = this.props.match.params.companyId;
    this.state = {
      orders: [],

    };
  }

// Connection with the API
  componentDidMount() {
    axios.get(API + this.companyId)
      .then(res => this.setState({
         orders: res.data
       }))

       .catch(error => this.setState({
           error,
           isLoading: false
       }));
  }


  render(){
    return(
      <main className="orders__overview">
      {<OrderBanner key={this.companyId} companyId={this.companyId} />}
      <section>
      {this.state.orders.map((order) => {
          return <OrderCard key={order.id} orderid={order.id} order={order.products_in_order} deliverytime={order.delivery_time} paymentmethod={order.payment_method} costs={order.costs} description={order.description} company_id={order.ordered_by_company_id}  />;
      })}
      </section>
      </main>
    );
  }

}

export default InComingOrders;
