import React from 'react'
import axios from 'axios'
import { Link } from "react-router-dom";

import './Register--Deliverer.css';

class Register extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      name: '',
      email: '',
      password: '',
      password_confirmation: ''
    }
  }

  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value});
  }

  //Sets correct role to latest registered user
  roleSetHandler = (id) => {
    axios.put('http://ipmedt4.applepi.nl/api/user/updaterole/deliverer/'+id)
    .then(response => {
      console.log(response);
    })
    .catch(error => {
      console.log(error);
    })
  }

  //Checks last added register
  roleHandler = e => {
    axios.get('http://ipmedt4.applepi.nl/api/returnusers')
    .then(response => {
      let value = response.data[response.data.length -1];
      console.log(value.id);
      this.roleSetHandler(value.id);
    })
    .catch(error => {
      console.log(error);
    })
  }

  //Posts a register to API
  submitHandler = e => {
    e.preventDefault()
    console.log(this.state)
    axios.post('http://ipmedt4.applepi.nl/api/registerDeliverer', this.state)
      .then(response => {
        console.log(response)
        this.roleHandler()
        window.location.replace("/login");
      })
      .catch(error => {
        console.log(error)
      })
  }



  render () {
    const { name, email, password, password_confirmation } = this.state
    return (
      <section className="register--section">
        <section className="register--section__section">
          <form className="register--section__section__from"onSubmit={this.submitHandler}>
            <img src="img/Finallogo.png" className="register--section__section__from_img" alt="Logo AlphensBezorgd"></img>
            <h3 className="register--section__section__from__h3">Bezorger registreren</h3>
            <input
            type="text"
            name="name"
            className="register--section__section__form__input"
            placeholder="naam"
            value={name}
            onChange={this.changeHandler}
            >
            </input>
              <input
              type="text"
              name="email"
              className="register--section__section__form__input"
              placeholder="email"
              value={email}
              onChange={this.changeHandler}
              >
              </input>
              <input
              type="password"
              name="password"
              className="register--section__section__form__input"
              placeholder="wachtwoord"
              value={password}
              onChange={this.changeHandler}
              >
              </input>
              <input
              type="password"
              name="password_confirmation"
              className="register--section__section__form__input"
              placeholder="bevestig wachtwoord"
              value={password_confirmation}
              onChange={this.changeHandler}
              >
              </input>
            <button type="submit" className="register--section__section__form__button">Register</button>
            <Link className="register--section__link" to="/login">Login</Link>
          </form>
        </section>
      </section>
    )
  }
}

export default Register;
