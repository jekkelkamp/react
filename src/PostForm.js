import React from 'react';
import axios from 'axios';
import './Form.css';


class PostForm extends React.Component{

  constructor(props){
    super(props)

    this.state = {
      customer_name: '',
      company_id: this.props.id,
      review_food: '',
      review_delivery: '',
      description: ''

    }
  }
//Handles event where input is post in post url
  changeHandler = (e) =>{
    this.setState({[e.target.name]: e.target.value})
  }

// Post hier het ingvulde form naar de api

  submitHandler = e => {
    e.preventDefault()
    console.log(this.state)
    axios.post('https://ipmedt4.applepi.nl/api/review/create', this.state)
    .then(response => {
      window.location.replace("/");
      console.log(response)
    })
    .catch(error =>{
      console.log(error)
    })
  }



  render() {
    const { customer_name, company_id, review_food, review_delivery, description } = this.state
    return (
        <section className="postform--addProduct">
        <form onSubmit = {this.submitHandler} className="postform--form">
        <h1> Review </h1>
          <section>
            <label for="customer_name" className="postform--label">Naam</label>
            <input
              className="postform--input__form"
              type="text"
              name="customer_name"
              value={customer_name}
              onChange={this.changeHandler}
              placeholder="Vul hier je naam in..."
              required
              />
          </section>
          <section>
            <label for="company_id" className="postform--label"> Bedrijfs ID </label>
            <input
            className="postform--input__form"
             type="number"
             name="company_id"
             value={company_id}
             onChange={this.changeHandler}
             placeholder="Cijfer"
             required
             />
          </section>
          <section>
            <label for="review_food" className="postform--label">Beoordeling bestelling</label>
            <input
              className="postform--input__form"
              type="number"
              name="review_food"
              value={review_food}
              onChange={this.changeHandler}
              placeholder="Cijfer"
              required
              />
          </section>
          <section>
            <label for="review_delivery" className="postform--label">Beoordeling levering </label>
            <input
              className="postform--input__form"
              type="number"
              name="review_delivery"
              value={review_delivery}
              onChange={this.changeHandler}
              placeholder="Cijfer"
              required
              />
          </section>
          <section>
            <label for="description" className="postform--label">Beschrijving </label>
            <textarea
              className="postform--textarea"
              type="text"
              name="description"
              value={description}
              onChange={this.changeHandler}
              placeholder="Beschrijving"
              required
              />
          </section>


          <button className="postform--btn__submit" type="submit">Submit</button>
        </form>
      </section>

    )
  }
}

export default PostForm;
