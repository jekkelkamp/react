//imports
import React from "react";

const FormNavbar = () => {
  return (
    <nav>
      <div className="navbar">

        <h3 className="navbar--title">AlphensBezorgd</h3>
      </div>
    </nav>
  );
};

export default FormNavbar;
