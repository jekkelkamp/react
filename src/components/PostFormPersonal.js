
import React, { Component } from 'react'
import axios from 'axios'
import "../Form.css";
import { withRouter } from 'react-router-dom';

class PostFormPersonal extends Component {

	constructor(props) {
		super(props)

		this.state = {
			name: '',
			email: '',
      phoneno: '',
			location_id: this.props.id,
			order_id: 1,
		}
	}
//Handles event where input is post in post url
	changeHandler = e => {
		this.setState({ [e.target.name]: e.target.value })
	}

//Handles event where input is post in post url
	submitHandler = e => {
		e.preventDefault()
		console.log(this.state)
		axios.post('http://ipmedt4.applepi.nl/api/customer/storeCustomer', this.state).then(response => {
				console.log(response)
				this.props.history.push('/orderingStep/3');
			})
			.catch(error => {
				console.log(error)
			})
	}
	

	render() {
		const {name, email, phoneno } = this.state
		return (

			<section  className="postform--addProduct">
      <form action="/orderingStep/2" onSubmit={this.submitHandler.bind(this)} className="postform--form" method="POST">

        <h3>Gegevens</h3>
        <section>
        <label for="name" className="postform--label">Naam</label>
        <input className="postform--input__form"
          type="text"
          name="name"
					value={name}
					onChange={this.changeHandler}/>

        </section>

        <section>
        <label for="email" className="postform--label">Email</label>
        <input
					className="postform--input__form"
          type="text"
          name="email"
					value={email}
					onChange={this.changeHandler}/>
        </section>

        <section>
        <label for="phoneno" className="postform--label">Telefoonnummer</label>
        <input
					className="postform--input__form"
          type="tel"
          name="phoneno"
					value={phoneno}
					onChange={this.changeHandler}/>
        </section>


				<button className="postform--btn__submit" type="submit">Bevestig</button>
				</form>
			</section>
		)
	}
}

export default withRouter(PostFormPersonal);
