import React from 'react';
import axios from 'axios';
import FormPayment from './Po';
import './RestaurantOverview.css';
import Navbar from './AddNavbar';


class GetCompany extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      companies: []
    };
  }

// Connection with the API
  componentDidMount() {
    axios.get('http://ipmedt4.applepi.nl/api/company').then(res => {
      this.setState({ companies: res.data });

    });
  }

  render(){
    return(
      <main>
      <Navbar />
      <article >
      {this.state.companies.map(company =>
        <FormPayment id={company.id} key={company.id} />
      )}

          </article>
      </main>
    );
  }

}



export default GetCompany;
