import React, { Component } from 'react'
import axios from 'axios'
import "../Form.css";
import productsInCart from "../cart-items";
import {connect} from "react-redux";
import {Link} from 'react-router-dom';
import { getCompanyId } from "../ProductOverview";
import getcart from './CartContainer';
import gettotal from './CartContainer';
import Select from 'react-select';
import cartItems from '../ProductOverview';


const optionsPayment = [
  { value: 'Contant', label: 'Contant' },
  { value: 'Ideal', label: 'Ideal' },
  { value: 'Creditcard', label: 'Creditcard' },
];

const OrderId = 1;

const optionsDeliveryTime = [
  { value: 'Zo snel mogelijk', label: 'Zo snel mogelijk' },
	{ value: '16:00', label: '16:00' },
  { value: '16:15', label: '16:15' },
  { value: '16:30', label: '16:30' },
	{ value: '16:45', label: '16:45' },
	{ value: '17:00', label: '17:00' },
  { value: '17:15', label: '17:15' },
  { value: '17:30', label: '17:30' },
	{ value: '17:45', label: '17:45' },
	{ value: '18:00', label: '18:00' },
  { value: '18:15', label: '18:15' },
  { value: '18:30', label: '18:30' },
	{ value: '18:45', label: '18:45' },
	{ value: '19:00', label: '19:00' },
  { value: '18:00', label: '18:00' },
  { value: '18:15', label: '18:15' },
  { value: '18:30', label: '18:30' },
	{ value: '18:45', label: '18:45' },
	{ value: '19:00', label: '19:00' },
	{ value: '19:15', label: '19:15' },
	{ value: '19:30', label: '19:30' },
	{ value: '19:45', label: '19:45' },
	{ value: '20:00', label: '20:00' },
	{ value: '20:15', label: '20:15' },
	{ value: '20:30', label: '20:45' },
];
let delivery = '';


class PaymentForm extends React.Component {

	constructor(props) {
		super(props)

		this.state = {
			products_in_order: 'test',
      orderded_by_company: productsInCart.company_id,
			costs: 10,
      title: '',
      delivery_time: '16:00',
      description: '',
      payment_complete: 0,

		}

	}

	changeHandler = e => {
		this.setState({ [e.target.name]: e.target.value })
	}
	state = {
    paymentmethod: null,
  };

  handleSelect = event => {
    this.setState({title: event.target.value})

  }

	handleChange = paymentmethod => {
    this.setState({ paymentmethod });
    console.log(`Option selected:`, paymentmethod.value);
    paymentmethod = paymentmethod.toString();
    return paymentmethod.value;
  };

	handleChangedeliverytime = deliverytime => {
		this.setState({ deliverytime });
		console.log(`Option selected:`, deliverytime.value);
    return deliverytime.value;


	};


	submitHandler = e => {
		e.preventDefault()
		console.log(this.state)
		axios.post('https://ipmedt4.applepi.nl/api/order/storeOrder' , this.state).then(response => {
				console.log(response)
			})
			.catch(error => {
				console.log(error)

			})
	}

	render() {
		const {products_in_order, costs, title, deliverytime, description, payment_complete } = this.state
		return (

      <section  className="addProduct">
        <form onSubmit={this.submitHandler} method="POST" className="form">
          <section>
          <h3>Bezorging</h3>
          <Select
              id="delivery_time"
              value={deliverytime}
              onChange={this.handleChangedeliverytime}
              options={optionsDeliveryTime}
              placeholder="Kies bezorgtijd"
          />
          </section>
            <section>
              <label for="description" className="label">Opmerkingen</label>
              <textarea className="input__form"
                name="description"
                rows="5" cols="20"
                value={description}
                onChange={this.changeHandler}
              />

          </section>
          <section>
          <h3>Betaalwijze</h3>
          <select className="input__form" value={this.state.title} onChange={this.handleSelect}>
          <option>Contant</option>
          <option>Ideal</option>
          <option>Creditcard</option>

          </select>

          </section>

          <input className="btn__submit" type="submit" value="Betalen"/>
        </form>
      </section>
		)
	}
}



export default PaymentForm;
