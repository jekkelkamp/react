//imports
import React from "react";

const FormNavbar = () => {
  return (
    <nav className="navbar">
      <div className="navbar--div">
        <h3 className="navbar--title">AlphensBezorgd</h3>
      </div>
    </nav>
  );
};

export default FormNavbar;
