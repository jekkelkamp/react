import React from 'react'
import axios from 'axios'
import "../Form.css";
import Select from 'react-select';
import { withRouter } from 'react-router-dom';



const optionsPayment = [
  { value: 'Contant', label: 'Contant' },
  { value: 'Ideal', label: 'Ideal' },
  { value: 'Creditcard', label: 'Creditcard' },
];


const optionsDeliveryTime = [
  { value: 'Zo snel mogelijk', label: 'Zo snel mogelijk' },
	{ value: '16:00', label: '16:00' },
  { value: '16:15', label: '16:15' },
  { value: '16:30', label: '16:30' },
	{ value: '16:45', label: '16:45' },
	{ value: '17:00', label: '17:00' },
  { value: '17:15', label: '17:15' },
  { value: '17:30', label: '17:30' },
	{ value: '17:45', label: '17:45' },
	{ value: '18:00', label: '18:00' },
  { value: '18:15', label: '18:15' },
  { value: '18:30', label: '18:30' },
	{ value: '18:45', label: '18:45' },
	{ value: '19:00', label: '19:00' },
  { value: '18:00', label: '18:00' },
  { value: '18:15', label: '18:15' },
  { value: '18:30', label: '18:30' },
	{ value: '18:45', label: '18:45' },
	{ value: '19:00', label: '19:00' },
	{ value: '19:15', label: '19:15' },
	{ value: '19:30', label: '19:30' },
	{ value: '19:45', label: '19:45' },
	{ value: '20:00', label: '20:00' },
	{ value: '20:15', label: '20:15' },
	{ value: '20:30', label: '20:45' },
];



class PostForm extends React.Component {

	constructor(props) {
		super(props)
//this part just won't work, but normally you would fill in the state with this.props.(whats supposed to be there)
		this.state = {
			products_in_order: 'BBQ Bacon & Chicken Pizza BBQ Bacon & Chicken Pizza Double Pepperoni',
      ordered_by_company_id: 1,
			costs: 27.97,
      payment_method: '',
      delivery_time: '',
      description: '',
      payment_complete: 0,

		}
	}

//Handles event where input is post in post url
	changeHandler = e => {
		this.setState({ [e.target.name]: e.target.value })
	}

//Handles event where input is post in post url
	handleChange = paymentmethod => {
    this.setState({ payment_method: paymentmethod.value })
    console.log(`Option selected:`, paymentmethod.value);
    paymentmethod = paymentmethod.toString();
    return paymentmethod.value;
  };
//Handles event where input is post in post url
	handleChangedeliverytime = deliverytime => {
		this.setState({ delivery_time: deliverytime.value });
		console.log(`Option selected:`, deliverytime.value);
    return deliverytime.value;

	};

//Handles event where input is post in post url
	submitHandler = e => {
		e.preventDefault()
		console.log(this.state)
		axios.post('https://ipmedt4.applepi.nl/api/order/storeOrder' , this.state).then(response => {
				console.log(response)
        this.props.history.push('/postform');
			})
			.catch(error => {
				console.log(error)

			})
	}


	render() {
	const { paymentmethod, deliverytime, description } = this.state
		return (
      <section  className="postform--addProduct">

        <form onSubmit={this.submitHandler} method="POST" className="postform--form" action="/postform">

          <h3>Bezorging</h3>
          <section className="postform--addProduct__section">
          <label for="bezorgtijd" className="postform--label">Bezorgtijd</label>
          <Select
              name="bezorgtijd"
              id="delivery_time"
              value={deliverytime}
              onChange={this.handleChangedeliverytime}
              options={optionsDeliveryTime}
              placeholder="Kies bezorgtijd"
          />
          </section>
            <section className="postform--addProduct__section">
              <label for="description" className="postform--label">Opmerkingen</label>
              <textarea
                className="postform--textarea"
                name="description"
                rows="5" cols="20"
                value={description}
                onChange={this.changeHandler}
              />

          </section>

          <h3>Betaalwijze</h3>
          <section className="postform--addProduct__section">
          <label for="betaalwijze" className="postform--label">Kies betaalwijze</label>

          <Select
              name="betaalwijze"
              id="delivery_time"
              value={paymentmethod}
              onChange={this.handleChange}
              options={optionsPayment}
              placeholder="Kies betaalmethode"
          />

          </section>

          <input className="postform--btn__submit" type="submit" value="Betalen"/>
        </form>

      </section>
		)
	}
}




export default withRouter(PostForm);
