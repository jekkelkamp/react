import "../Form.css";
import { withRouter } from 'react-router-dom';
import React from 'react'
import axios from 'axios'

class PostFormAdress extends React.Component{

  constructor(props){
    super(props)

    this.state = {
      streetname: '',
      house_number: '',
      house_number_addition: '',
      postal_code: '',

    }
  }
//Handles event where input is post in post url
  changeHandler = (e) =>{
    this.setState({[e.target.name]: e.target.value})
  }
//Handles event where input is post in post url
  submitHandler = e => {
    e.preventDefault()
    console.log(this.state)
    axios.post('https://ipmedt4.applepi.nl/api/location/storeLocation', this.state)
    .then(response => {
      console.log(response)
			this.props.history.push('/orderingStep/2');
    })
    .catch(error =>{
      console.log(error)
    })
  }

  render() {
    const { streetname, house_number, house_number_addition, postal_code } = this.state
    return (
      <section  className="postform--addProduct">
        <form action="/orderingStep/2" className="postform--form" method="POST" onSubmit={this.submitHandler.bind(this)}>
				<h3>Bezorgadres</h3>
          <section>
					<label for="streetname" className="postform--label">Straatnaam</label>
            <input className="postform--input__form"
              type="text"
              name="streetname"
              value={streetname}
              onChange={this.changeHandler}
              />
          </section>
          <section>
					<label for="house_number" className="label">Straatnummer</label>
            <input className="postform--input__form"
             type="number"
             name="house_number"
             value={house_number}
             onChange={this.changeHandler}
             />
          </section>
          <section>
					<label for="housenumberaddition" className="label">Straatnummer toevoeging</label>
            <input className="postform--input__form"
              type="text"
              name="house_number_addition"
              value={house_number_addition}
              onChange={this.changeHandler}
              />
          </section>
          <section>
					<label for="postal_code" className="label">Postcode</label>
					<input className="postform--input__form"
						type="text"
						name="postal_code"
						value={postal_code}
						onChange={this.changeHandler}
						/>
          </section>



          <button className="postform--btn__submit" type="submit">Bevestig</button>
        </form>
      </section>
    )
  }
}

export default withRouter(PostFormAdress);
