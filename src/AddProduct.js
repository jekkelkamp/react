import React from 'react';
import './AddProduct.css';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

class AddProduct extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      name: '',
      description: '',
      allergies: '',
      price: '',
      company_id: this.props.location.state.companyId,
    }
  }

//Handles event where input is post in post url
  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value })
  }

//Handles event where input is post in post url
  submitHandler = e => {
    e.preventDefault()
    console.log(this.state)
    axios.post('http://ipmedt4.applepi.nl/api/product/storeProduct', this.state)
    .then(response => {
      console.log(response)
      this.props.history.push('/addtoproduct');
    })
    .catch(error => {
      console.log(error)
    })
  }

  render() {
    console.log(this.props.id);
		const {name, description, allergies, price} = this.state
		return (

			<section className="postform--addProduct">
      <form action="/" onSubmit={this.submitHandler.bind(this)} className="postform--form" method="POST">

      <section className="postform--addProduct__section">
        <h3>Producten Gegevens</h3>
        <label for="name" className="postform--label">Naam</label>
        <input
          className="postform--input__form"
          type="text"
          name="name"
					value={name}
					onChange={this.changeHandler}/>
        </section>

        <section className="postform--addProduct__section">
        <label for="description" className="postform--label">Beschrijving</label>
        <input
          className="postform--input__form"
          type="text"
          name="description"
					value={description}
					onChange={this.changeHandler}/>
        </section>

        <section className="postform--addProduct__section">
        <label for="allergies" className="postform--label">Allergieën</label>
        <input
          className="postform--input__form"
          type="text"
          name="allergies"
					value={allergies}
					onChange={this.changeHandler}/>
        </section>

        <section className="postform--addProduct__section">
        <label for="price" className="postform--label">Prijs</label>
        <input
          className="postform--input__form"
          type="number"
          name="price"
					value={price}
					onChange={this.changeHandler}/>
        </section>


				<button className="postform--btn__submit" type="submit">Bevestig</button>
				</form>
			</section>
		)
	}
}

export default withRouter(AddProduct);
