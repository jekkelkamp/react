import React from 'react';
import PostFormPersonal from './components/PostFormPersonal';
import Navbar from "./components/FormPersonalNavbar";
import axios from "axios";



class OrderForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      locations: []
    };
  }

// Connection with the API
  componentDidMount() {
    axios.get('http://ipmedt4.applepi.nl/api/locations').then(res => {
      let last = res.data[res.data.length -1];
      this.setState({ locations: last});
      console.log(last);


    });
  }


  render() {
    return(
			<section className="OrderForm">
        <Navbar />
        <section>
        <PostFormPersonal id={this.state.locations.id} key={this.state.locations.id} />


			</section>
			</section>
		)
  }
}

export default OrderForm;
