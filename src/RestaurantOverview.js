import React from 'react';
import axios from 'axios';
import './RestaurantOverview.css';
import RestaurantCard from "./RestaurantCard";

import Navbar from './NavBar';

import SearchBar from "./SearchBar";

const API = "http://ipmedt4.applepi.nl/api/company";

// This Component makes a flexbox page and will be call in de RestaurantPage.js were the Navbar will be added
class RestaurantOverview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      companies: [],
      isLoading: false,
      error: null,
    };
  }

  // Connection with the API
  componentDidMount() {
    this.getAllCompanies();
  }

  // Gets all companies out API
  getAllCompanies() {
    axios.get(API + "/by-company-name").then(res => {
      this.setState({ companies: res.data });
    });
  }

// Searchbar function that makes you search in the companies list
  onSearch = (restaurantName) => {
    if (!restaurantName) {
      this.getAllCompanies();
    } else {
      this.setState({ isLoading: true });
      axios.get(API + "/like-company-name/" + restaurantName)
        .then(res => this.setState({
          companies: res.data
        }))
        .catch(error => this.setState({
          companies: []
        }));
    }
  }

        render() {
          return (
          <main>
          <Navbar />

          <article>
          <section className="restaurantoverview_searchbar">
            <SearchBar onSearch={this.onSearch} />
          </section>

          <section className="restaurantoverview">
            {this.state.companies.map(company =>
              <RestaurantCard title={company.name} id={company.id} key={company.id} content="Lekker" imgSrc={company.image_location}/>
            )}
            </section>
          </article>

          </main>


    );
  }

}

export default RestaurantOverview;
