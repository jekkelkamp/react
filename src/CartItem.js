import React, { Component } from 'react';

import "./Cartitem.css"

class CartItem extends Component{

  render(){

    return(
      <section className="cartitem--section">
        <section className="cartitem--section__amount">
          {/* amount */}
          <p className="cartitem--amount">{this.props.amount}</p>
        </section>
        <section>
          <section className="cartitem--section__title">
            <h4 className="cartitem--title">{this.props.name}</h4>
            {/* remove button */}
          </section>

          <h4 className="cartitem--price">€{this.props.price}</h4>
          <button className="cartitem--btn__remove" onClick = {this.props.functionRemove}  >Verwijderen</button>
        </section>
        <section>
          {/* decrease amount*/}
          <button className="cartitem--btn__decrease" onClick = {this.props.functionDown}>
              <svg className="cartitem--btn__decrease__svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <path d="M0 10h24v4h-24z"/></svg>
            </button>
          {/* increase amount */}
          <button className="cartitem--btn__increase" onClick = {this.props.functionUp} >
          <svg className="cartitem--btn__increase__svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
          <path d="M24 10h-10v-10h-4v10h-10v4h10v10h4v-10h10z"/></svg>
          </button>
        </section>
      </section>
    );
  }
}

export default CartItem;
