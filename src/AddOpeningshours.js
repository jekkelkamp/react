import React from 'react';
import Openingshours from './Openingshours';
import Navbar from './LoginNavbar';
import './RestaurantOverview.css';


// This Component makes a flexbox page and will be call in de RestaurantPage.js were the Navbar will be added
class AddOpeningshours extends React.Component {
    render() {
        return (
            <main className="background">
                <article>
                    <Navbar />
                    <article >
                        <Openingshours />

                    </article>
                </article>
            </main>
        );
    }

}

export default AddOpeningshours;
