import React from 'react';
import './RestaurantBanner.css';

import axios from 'axios';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import FeedbackIcon from '@material-ui/icons/Feedback';
import CloseIcon from '@material-ui/icons/Close';


class Popup extends React.Component {
    render() {
        return (
            <article className='popup'>
                <section className='popup_content'>
                    <header className="popup_content_header">
                        <h1>{this.props.title} </h1>
                        <button onClick={this.props.closePopup} className="popup_content_header_closebutton"><CloseIcon fontSize="large" /></button>
                    </header>
                    {this.props.child}
                </section>
            </article>
        );
    }
}

class LocationPopup extends React.Component {
    render() {
        return (
            <section className="address_container">
                <p>Straatnaam:</p>
                <p className="address_container_addressdata">{this.props.streetName}</p>
                <p>Huisnummer:</p>
                <p className="address_container_addressdata">{this.props.houseNumber}{this.props.houseNumberAddition}</p>
                <p>Postcode:</p>
                <p className="address_container_addressdata">{this.props.postalCode}</p>
            </section>
        );
    }
}

class TimesPopup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            monday: "",
            tuesday: "",
            wednesday: "",
            thursday: "",
            friday: "",
            saturday: "",
            sunday: "",
            isLoading: false,
            error: null,
        };
    }


    // Connection with the API
    componentDidMount() {
        axios.get("https://ipmedt4.applepi.nl/api/company/by-company-id/" + this.props.companyId)
            .then(res => this.setState({
                monday: res.data.opening_hour.monday,
                tuesday: res.data.opening_hour.tuesday,
                wednesday: res.data.opening_hour.wednesday,
                thursday: res.data.opening_hour.thursday,
                friday: res.data.opening_hour.friday,
                saturday: res.data.opening_hour.saturday,
                sunday: res.data.opening_hour.sunday,
                isLoading: false
            }))

            // Display error message when the API data could not be fetched.
            .catch(error => this.setState({
                products: [],
                isLoading: false,
                error
            }));
    }

    render() {
        return (
            <section className="times_container">
                <p>Maandag:</p>
                <p className="times_container_openinghours">{this.state.monday}</p>
                <p>Dinsdag:</p>
                <p className="times_container_openinghours">{this.state.tuesday}</p>
                <p>Woensdag:</p>
                <p className="times_container_openinghours">{this.state.wednesday}</p>
                <p>Donderdag:</p>
                <p className="times_container_openinghours">{this.state.thursday}</p>
                <p>Vrijdag:</p>
                <p className="times_container_openinghours">{this.state.friday}</p>
                <p>Zaterdag:</p>
                <p className="times_container_openinghours">{this.state.saturday}</p>
                <p>Zondag:</p>
                <p className="times_container_openinghours">{this.state.sunday}</p>
            </section>
        );
    }
}



// const REVIEWS = [
//     { id: 1, customer_id: "Henk de Jong", company_id: "1", review_food: "7.7", review_delivery: "8", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", created_at: "10-06-2020", updated_at: "1", },
//     { id: 2, customer_id: "Arie Jansen", company_id: "2", review_food: "9", review_delivery: "6.5", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", created_at: "10-06-2020", updated_at: "2", },
//     { id: 3, customer_id: "Frits van den Berg", company_id: "3", review_food: "6.5", review_delivery: "5.5", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", created_at: "10-06-2020", updated_at: "3", },
//     { id: 4, customer_id: "Karen Mulder", company_id: "4", review_food: "2", review_delivery: "1.5", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", created_at: "10-06-2020", updated_at: "4", },
// ];

class ReviewsPopup extends React.Component {
  constructor(props) {
      super(props);

      this.state = {
        reviews: []
      };
    }

  // Connection with the API
    componentDidMount() {
      axios.get("http://ipmedt4.applepi.nl/api/review/by-company-id/" + this.props.companyId).then(res => {
        this.setState({ reviews: res.data});
      });
    }

    render() {
        console.log(this.state);
        return (
            // Loop over all test review data and create cards.
            <section>
                {this.state.reviews.map((review) => {
                  return(
                        <article key={review.id} className="reviewcards">
                            <header className="reviewcards_header">
                                <p className="review_cards_header_customerName">{review.customer_name}</p>
                                <p className="reviewcards_header_timestamp">{review.created_at}</p>
                            </header>
                            <section className="reviewcards_review">
                                <section className="reviewcards_review_ratings">
                                    <p>Eten: {review.review_food}</p>
                                    <p className="reviewcards_review_ratings_deliverrating">Bezorging: {review.review_delivery}</p>
                                </section>
                                <p>{review.description}</p>
                            </section>
                        </article>
                      );
                })}
            </section>
        );
    }
}


const API = "https://ipmedt4.applepi.nl/api/company/by-company-id/";

class RestaurantBanner extends React.Component {
    constructor() {
        super();
        this.state = {
            showInformationPopup: false,
            showTimesPopup: false,
            showReviews: false,
            companyName: null,
            streetName: null,
            houseNumber: null,
            houseNumberAddition: null,
            postalCode: null,
            isLoading: false,
            error: null,
            companyId: null
        };
    }

    componentDidMount() {
        this.setState({ isLoading: true });

        axios.get(API + this.props.companyId)
            .then(res => this.setState({
                companyName: res.data.name,
                streetName: res.data.locations.streetname,
                houseNumber: res.data.locations.house_number,
                houseNumberAddition: res.data.locations.house_number_addition,
                postalCode: res.data.locations.postal_code,
                isLoading: false,
                companyId: res.data.id
            }))
            // Display error message when the API data could not be fetched.
            .catch(error => this.setState({
                error,
                isLoading: false
            }));
    }

    toggleInformationPopup() {
        this.setState({
            showInformationPopup: !this.state.showInformationPopup
        });
    }

    toggleTimesPopup() {
        this.setState({
            showTimesPopup: !this.state.showTimesPopup
        });
    }

    toggleReviewsPopup() {
        this.setState({
            showReviews: !this.state.showReviews
        });
    }

    render() {
        const locationPopup = <LocationPopup
            streetName={this.state.streetName}
            houseNumber={this.state.houseNumber}
            houseNumberAddition={this.state.houseNumberAddition}
            postalCode={this.state.postalCode}
        />

        const timesPopup = <TimesPopup
            companyId={this.props.companyId}
        />

        const reviewsPopup = <ReviewsPopup
            times={this.state.times}
            companyId = {this.props.companyId}
        />


        return (
            <header className="restaurant_header">
                <h1>{this.state.companyName || "Restaurantnaam"}</h1>

                <section className="restaurant_header_section">
                    <button onClick={this.toggleInformationPopup.bind(this)} className="restaurant_header_section_button"><LocationOnIcon /></button>
                    <button onClick={this.toggleTimesPopup.bind(this)} className="restaurant_header_section_button"><AccessTimeIcon /></button>
                    <button onClick={this.toggleReviewsPopup.bind(this)} className="restaurant_header_section_button"><FeedbackIcon /></button>
                </section>
                {this.state.showInformationPopup ? <Popup title='Adresgegevens' closePopup={this.toggleInformationPopup.bind(this)} child={locationPopup} /> : null}
                {this.state.showTimesPopup ? <Popup title='Openingstijden' closePopup={this.toggleTimesPopup.bind(this)} child={timesPopup} /> : null}
                {this.state.showReviews ? <Popup title='Klantreviews' closePopup={this.toggleReviewsPopup.bind(this)} child={reviewsPopup} /> : null}
            </header>
        );
    }
}

export default RestaurantBanner;
