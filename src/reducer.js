
import {DECREASE, INCREASE, CLEAR_CART, REMOVE, GET_TOTALS} from './actions';

//Function for clearing cart
const reducer = (state, action) =>{
  if(action.type === CLEAR_CART){
    return {...state, cart: []};
  }

  //function for decreasing cart
  if(action.type === DECREASE){
    let tempCart = [];
    //if cart is equal to one it wil remove it from cart
    if(action.payload.amount === 1){
      tempCart = state.cart.filter(cartItem => cartItem.id !== action.payload.id);
    }
    else {
      tempCart = state.cart.map((cartItem) => {
        if(cartItem.id === action.payload.id){
          cartItem = {...cartItem, amount:cartItem.amount - 1};
        }
        return cartItem;
      });
    }
    return {...state,cart:tempCart}
  }

  //function for increase the amount in cart. it checks the state, copies that and multiple by one.
  if(action.type === INCREASE){
    let tempCart = state.cart.map((cartItem) => {
      if(cartItem.id === action.payload.id){
        cartItem = {...cartItem, amount:cartItem.amount + 1};
      }
      return cartItem;
    });
    return{ ...state,cart:tempCart };
  }
  //function for remove, if remove button is clicked it will remove from cart
  if(action.type === REMOVE){
    return {...state,cart:state.cart.filter((cartItem) => cartItem.id !== action.payload.id
  )};
  }

//function for getting the total of cart
  if(action.type === GET_TOTALS){
    let { total, amount } = state.cart.reduce(
      (cartTotal, cartItem) => {
        const {price, amount} = cartItem;
        const itemTotal = price * amount;
        cartTotal.total += itemTotal;
        cartTotal.amount += amount;

      return cartTotal;
    }, {
      total:0,
      amount: 0
    });
    //makes sure the total is two decimals
    total = parseFloat(total.toFixed(2));
    return {...state, total, amount }
  }
  return state;
}


export default reducer;
