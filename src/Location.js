import React from 'react';
import './AddProduct.css';
import axios from 'axios';
import { withRouter } from 'react-router-dom';


class Location extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      streetname: '',
      house_number: '',
      house_number_addition: '',
      postal_code: ''
    }
  }

  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value })
  }

  submitHandler = e => {
    e.preventDefault();
    axios.post('http://ipmedt4.applepi.nl/api/location/storeLocation', this.state)
      .then(response => {
        this.props.history.push({ pathname: '/companies', state: { locationId: response.data.id } });
      })
      .catch(error => {
        console.log(error);
      })
  }

  render() {
    const { streetname, house_number, house_number_addition, postal_code } = this.state
    return (
      <section className="postform--addProduct">
        <form action="/" onSubmit={this.submitHandler.bind(this)} className="postform--form" method="POST">

          <section className="postform--addProduct__section">
            <h3>Bedrijf locatie gegevens</h3>

            <label for="streetname" className="postform--label">Straatnaam</label>
            <input
              className="postform--input__form"
              type="text"
              name="streetname"
              value={streetname}
              onChange={this.changeHandler} />

          </section>

          <section className="postform--addProduct__section">
            <label for="house_number" className="postform--label">Huisnummer</label>
            <input
              className="postform--input__form"
              type="number"
              name="house_number"
              value={house_number}
              onChange={this.changeHandler} />
          </section>

          <section className="postform--addProduct__section">
            <label for="house_number_addition" className="postform--label">Huisnummer toevoeging</label>
            <input
              className="postform--input__form"
              type="text"
              name="house_number_addition"
              value={house_number_addition}
              onChange={this.changeHandler} />
          </section>

          <section className="postform--addProduct__section">
            <label for="postal_code" className="postform--label">Postcode</label>
            <input
              className="postform--input__form"
              type="text"
              name="postal_code"
              value={postal_code}
              onChange={this.changeHandler} />
          </section>

          <button className="postform--form__btn__order" type="submit">Bevestig</button>
        </form>
      </section>
    )
  }
}

export default withRouter(Location);
