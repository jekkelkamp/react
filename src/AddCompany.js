import React from 'react';
import './AddProduct.css';
import axios from 'axios';
import { withRouter } from 'react-router-dom';


class AddCompany extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      name: '',
      location_id: this.props.id,
      image_location: '',
    }
  }
  //Handles event where input is post in post url
  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value })
  }


  previewFile(){
    const preview = document.getElementById('addCompany__previewPhoto__photo');
    const file =  document.getElementById('input__file').files[0];
    const reader = new FileReader();

    // Hier laat je de foto zien
    reader.addEventListener('load', function(){
      preview.src=reader.result;

    },false);

    if(file != null){
      reader.readAsDataURL(file);

  // De file wordt omgezet naar een base64 bestand
      reader.onload = () => {
        var base64 = reader.result;
        var base64_string = base64.toString();
        this.setState({
          image_location: base64_string
        })
      }
    }
  }

  submitHandler = e => {
    e.preventDefault();
    axios.post('https://ipmedt4.applepi.nl/api/company/storeCompany', this.state)
      .then(response => {
        this.props.history.push({ pathname: '/addopeningshours', state: { companyId: response.data.id } });
      })
      .catch(error => {
        console.log(error);
      })
  }


  render() {

  	const {name} = this.state
    return (

      <section className="postform--addProduct">
        <form action="http://ipmedt4.applepi.nl/api/company/storeCompany" onSubmit={this.submitHandler.bind(this)} className="postform--form" method="POST">

          <section className="postform--addProduct__section">
            <h3>Bedrijf</h3>

            <label for="name" className="postform--label">Bedrijfsnaam</label>
            <input className="postform--input__form"
              type="text"
              name="name"
              value={name}
              onChange={this.changeHandler}/>
          </section>

          <label forhtml="photo" className="label">Foto</label>
          <input className="input__form"
            id = "input__file"
            type="file"
            name="photo"
            onChange={() => this.previewFile()}/>
            <section className="addCompany__previewPhoto">
              <img
                src="" id="addCompany__previewPhoto__photo"
                alt="bedrijfslogo"
              />
            </section>


          <button className="postform--btn__submit" type="submit">Bevestig</button>
        </form>
      </section>
    )
  }
}

export default withRouter(AddCompany);
