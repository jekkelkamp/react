import React, { Component } from 'react'
import axios from 'axios'
import { Link } from "react-router-dom";

import './Login.css';

class Login extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      email: '',
      password: ''
    }
  }
//Handles event where input is post in post url
  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value})
  }

//Handles event where input is post in post url
  submitHandler = e => {
    e.preventDefault()
    console.log(this.state)
    axios.post('http://ipmedt4.applepi.nl/api/login', this.state)
      .then(response => {
        console.log(response)
      })
      .catch(error => {
        console.log(error)
      })
  }

  apiHandler = () => {
    console.log("apiHandler wordt aangeroepen")
  }

  render () {
    const { email, password } = this.state
    return (
      <section className="login--section">
        <section className="login--section__section">
          <form className="login--section__section__from" onSubmit={this.submitHandler}>
            <img src="img/Finallogo.png" className="login--section__section__from__img" alt="Logo AlphensBezorgd"></img>
            <h3 className="login--section__section__from__h3">Login</h3>
            <input
            type="text"
            name="email"
            className="login--section__section__form__input"
            value={email}
            onChange={this.changeHandler}
            >
            </input>
            <input
            type="password"
            name="password"
            className="login--section__section__form__input"
            value={password}
            onChange={this.changeHandler}
            >
            </input>
            <button type="submit" className="login--section__section__form__button" onSubmit={this.apiHandler}>Login</button>
            <Link class="login--section__link" to="/register">Registreer als klant</Link>
          </form>
        </section>
      </section>
    )
  }
}

export default withRouter(Login);
