import React from 'react';
import './AddProduct.css';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

class Openingshours extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            monday: '',
            tuesday: '',
            wednesday: '',
            thursday: '',
            friday: '',
            saturday: '',
            sunday: '',

        }
    }

    changeHandler = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    submitHandler = e => {
        e.preventDefault()
        console.log(this.state)
        axios.post('http://ipmedt4.applepi.nl/api/openingshours/storeOpeningshours', this.state)
            .then(response => {
                console.log(response)
                this.props.history.push('/companies');
            })
            .catch(error => {
                console.log(error)
                this.props.history.push('/companies');
            })
    }




    render() {
        const { monday, tuesday, wednesday, thursday, friday, saturday, sunday } = this.state
        return (
            <section className="postform--addProduc">
                <form action="/" onSubmit={this.submitHandler.bind(this)} className="postform--form" method="POST">

                    <section className="postform--addProduct__section">
                        <h3>Bedrijf Openingstijden</h3>

                        <label for="monday" className="postform--label">Maandag</label>
                        <input
                            className="postform--input__form"
                            type="text"
                            name="monday"
                            placeholder="Bijvoorbeeld: 08:00 - 22:00"
                            value={monday}
                            onChange={this.changeHandler} />

                        <label for="tuesday" className="postform--label">Dinsdag</label>
                        <input
                            className="postform--input__form"
                            type="text"
                            name="tuesday"
                            placeholder="Bijvoorbeeld: 08:00 - 22:00"
                            value={tuesday}
                            onChange={this.changeHandler} />

                        <label for="wednesday" className="postform--label">Woensdag</label>
                        <input
                            className="postform--input__form"
                            type="text"
                            name="wednesday"
                            placeholder="Bijvoorbeeld: 08:00 - 22:00"
                            value={wednesday}
                            onChange={this.changeHandler} />

                        <label for="thursday" className="postform--label">Donderdag</label>
                        <input
                            className="postform--input__form"
                            type="text"
                            name="thursday"
                            placeholder="Bijvoorbeeld: 08:00 - 22:00"
                            value={thursday}
                            onChange={this.changeHandler} />

                        <label for="friday" className="postform--label">Vrijdag</label>
                        <input
                            className="postform--input__form"
                            type="text"
                            name="friday"
                            placeholder="Bijvoorbeeld: 08:00 - 22:00"
                            value={friday}
                            onChange={this.changeHandler} />

                        <label for="saturday" className="postform--label">Zaterdag</label>
                        <input
                            className="postform--input__form"
                            type="text"
                            name="saturday"
                            placeholder="Bijvoorbeeld: 08:00 - 22:00"
                            value={saturday}
                            onChange={this.changeHandler} />

                        <label for="sunday" className="postform--label">Zondag</label>
                        <input
                            className="postform--input__form"
                            type="text"
                            name="sunday"
                            placeholder="Bijvoorbeeld: 08:00 - 22:00"
                            value={sunday}
                            onChange={this.changeHandler} />

                    </section>



                    <button className="postform--btn__submit" type="submit">Bevestig</button>
                </form>
            </section>

        )
    }
}

export default withRouter(Openingshours);
